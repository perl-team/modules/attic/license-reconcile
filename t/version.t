#!/usr/bin/perl -wl

use Test::More;
use File::Basename;

my $basedir = dirname($0).'/..';

my $debian_changelog_version = `head -1 $basedir/debian/changelog | awk -F'[ ()]+' '{print \$2}'`;
chomp($debian_changelog_version);

foreach my $file (map { "$basedir/$_" }
                  ('lib/Debian/LicenseReconcile.pm',
                   glob('lib/Debian/LicenseReconcile/*.pm'),
                   glob('lib/Debian/LicenseReconcile/*/*.pm'))) {
    my $pm_version = `egrep '^our .VERSION' $file`;
    eval($pm_version);

    is( $VERSION, $debian_changelog_version,
        "Version number in debian/changelog and \$VERSION in $file are the same" );

    my $pod_version = `egrep '^Version [0-9]' $file`;
    my @v = split(/\s+/, $pod_version);

    is( $v[1], $debian_changelog_version,
        "Version numbers in debian/changelog and POD of $file are the same" );
}

done_testing;
