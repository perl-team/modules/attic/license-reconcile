#!/usr/bin/perl

use Test::More tests=>3;
use Test::Deep;
use Test::NoWarnings;
use Debian::LicenseReconcile::LicenseCheck;
use Readonly;
use File::Temp qw();
use Software::LicenseMoreUtils;
use Data::Dumper;
require UNIVERSAL::require;

Readonly my @LICENSES => qw(
    Apache_2_0
    FreeBSD
    GPL_1
    GPL_2
    GPL_3
    LGPL_2
    LGPL_2_1
    LGPL_3_0
    MIT
    Mozilla_2_0
    QPL_1_0
    Zlib
);

    #CC0_1_0
    #GFDL_1_3
    #Artistic_1_0
    #Artistic_2_0
    #Mozilla_1_0
    #None
    #PostgreSQL
    #AGPL_3
    #SSLeay
    #Apache_1_1
    #Mozilla_1_1
    #GFDL_1_2
    #Sun
    #BSD
    #OpenSSL
    #Perl_5

my $dir = File::Temp->newdir('t/slgenXXXXX');
my $year = 2000;
foreach my $license (@LICENSES) {
	my $slfile = $dir->dirname . "/$license";
	my $slobj = Software::LicenseMoreUtils->new_from_short_name({
		holder => 'Testophilus Testownik <tester@testity.org>',
		year=>$year,
		short_name => $license,
	});
	open my $fh, '>', $slfile;
	print $fh $slobj->summary_or_text;
	close $fh;
	++$year;
}

Readonly my $LICENSECHECK => Debian::LicenseReconcile::LicenseCheck->new(
	$dir->dirname,
	[
		# Want to test that we can map if we need to.
		'Apache (v2.0)'=>'test1',
		'BSD (2 clause)'=>'test2',
		'GPL (v1)'=>'test3',
		'GPL (v2)'=>'test4',
		'GPL (v3)'=>'test5',
		'LGPL (v2)'=>'test6',
		'LGPL (v2.1)'=>'test7',
		'LGPL (v3)'=>'test8',
		'MIT/X11 (BSD like)'=>'test9',
		'MPL (v2.0)'=>'test10',
		'QPL (v1.0)'=>'test11',
		'zlib/libpng'=>'test12',
	],
	1
);

isa_ok($LICENSECHECK, 'Debian::LicenseReconcile::LicenseCheck');
my @data = $LICENSECHECK->get_info;
print Dumper(@data);
cmp_deeply(\@data, bag(
    {
        file=>'Apache_2_0',
        license=>'test1',
	copyright=>['Copyright: 2000 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'FreeBSD',
        license=>'test2',
	copyright=>['Copyright: 2001 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'GPL_1',
        license=>'test3',
	copyright=>['Copyright: 2002 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'GPL_2',
        license=>'test4',
	copyright=>['Copyright: 2003 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'GPL_3',
        license=>'test5',
	copyright=>['Copyright: 2004 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'LGPL_2',
        license=>'test6',
	copyright=>['Copyright: 2005 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'LGPL_2_1',
        license=>'test7',
	copyright=>['Copyright: 2006 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'LGPL_3_0',
        license=>'test8',
	copyright=>['Copyright: 2007 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'MIT',
        license=>'test9',
	copyright=>['Copyright: 2008 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'Mozilla_2_0',
        license=>'test10',
	copyright=>['Copyright: 2009 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'QPL_1_0',
        license=>'test11',
	copyright=>['Copyright: 2010 Testophilus Testownik <tester@testity.org>.'],
    },
    {
        file=>'Zlib',
        license=>'test12',
	copyright=>['Copyright: 2011 Testophilus Testownik <tester@testity.org>.'],
    },
));
